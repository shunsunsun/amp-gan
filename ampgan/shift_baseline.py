"""
Creates baseline amino acid frequency distribution plots using randomly constructed sequences.
"""

import logging
import random
from typing import List, Optional

import numpy as np
import seaborn as sns
from analyze_generated_samples import (
    amino_acid_frequency_comparison,
    tokenize_sequences,
)
from shifterator import EntropyShift

logger = logging.getLogger(__name__)

common_aas: List[str] = [
    "A",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "K",
    "L",
    "M",
    "N",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "V",
    "W",
    "Y",
]


def main(
    n_samples: int = 5120,
    min_len: int = 1,
    max_len: int = 32,
    characters: Optional[List[str]] = None,
):
    characters = characters or common_aas
    char_count = len(characters)
    group_1, group_2 = (
        generate_random_sequences(
            n_samples, characters=characters, min_len=min_len, max_len=max_len
        ),
        generate_random_sequences(
            n_samples, characters=characters, min_len=min_len, max_len=max_len
        ),
    )

    sns.set(style="white", palette=None)

    for window_size in range(1, 4):
        counts_1 = tokenize_sequences(group_1, window_size)
        counts_2 = tokenize_sequences(group_2, window_size)
        logger.info(
            f"Maximum entropy for {window_size}-grams: {-np.log2(1 / char_count ** window_size):0.6f}"
        )
        logger.info(
            f"{window_size}-grams observed in group 1: "
            f"{len(counts_1)} / {char_count ** window_size} = {len(counts_1) / char_count ** window_size:0.6f}"
        )
        logger.info(
            f"{window_size}-grams observed in group 2: "
            f"{len(counts_2)} / {char_count ** window_size} = {len(counts_2) / char_count ** window_size:0.6f}"
        )

        if window_size == 1:
            sns.set_style("whitegrid")
            amino_acid_frequency_comparison(
                counts_1, counts_2, tag=f"shift_baseline_{window_size}"
            )
        else:
            sns.reset_orig()
            EntropyShift(counts_1, counts_2).get_shift_graph(
                system_names=["Group 1", "Group 2"],
                filename=f"../results/shift_baseline_{window_size}.png",
                show_plot=False,
                text_size_inset=False,
            )


def generate_random_sequences(
    count: int, characters: List[str], min_len: int, max_len: int,
) -> List[str]:
    """
    Args:
        count: Number of sequences to be generated.
        characters: Character set used to construct sequences.
        min_len: Smallest allowable sequence.
        max_len: Largest allowable sequence.
    Returns: List of randomly generated sequences
    """
    sequences = []
    for _ in range(count):
        length = random.randint(min_len, max_len + 1)
        seq = "".join(random.choices(characters, k=length))
        sequences.append(seq)
    return sequences


if __name__ == "__main__":
    logging.basicConfig(
        filename=f"../results/shift_baseline.log",
        filemode="w",
        level=logging.INFO,
        format="%(message)s",
    )

    main()
