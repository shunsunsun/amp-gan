import logging
import multiprocessing
from itertools import zip_longest
from typing import List, Optional, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from analyze_generated_samples import sequence_similarity_report
from shift_baseline import common_aas, generate_random_sequences

logger = logging.getLogger(__name__)


def main(
    n_samples: int = 1000,
    min_len: int = 1,
    max_len: int = 32,
    characters: Optional[List[str]] = None,
    replicates: int = 30,
):
    """~25 minute runtime"""
    characters = characters or common_aas

    args = [
        (frac, n_samples, characters, min_len, max_len)
        for _ in range(replicates)
        for frac in np.linspace(0.0, 1.0, num=51, endpoint=True)
    ]

    with multiprocessing.Pool() as pool:
        results = pool.starmap(probe_scoring, args)

    mix, match_scores = zip(*results)
    mix = np.concatenate(mix)
    match_scores = np.concatenate(match_scores)
    df = pd.DataFrame({"Mixture Parameter": mix, "Match Score": match_scores})

    sns.set_style("whitegrid")
    sns.lineplot(data=df, x="Mixture Parameter", y="Match Score", ci="sd")
    plt.savefig("../results/probe_global_scores.png")

    describe_plot(data=df, x="Mixture Parameter", y="Match Score")
    plt.savefig("../results/probe_global_scores_alt.png")
    plt.close()

    logger.info(f"Match Score Summary:\n{df.describe()}")


def probe_scoring(
    frac: float, n_samples: int, characters: List[str], min_len: int, max_len: int,
) -> Tuple[np.ndarray, pd.Series]:
    rand_seqs = generate_random_sequences(n_samples // 2, characters, min_len, max_len)
    dupe_set = rand_seqs + rand_seqs
    unique_set = generate_random_sequences(n_samples, characters, min_len, max_len)

    seqs_mixed = [mix_sequences(a, b, frac) for a, b in zip(dupe_set, unique_set)]
    mixed_df = pd.DataFrame({"sequence": seqs_mixed})
    similarity = sequence_similarity_report(mixed_df, mixed_df, skip_identical=True)
    return frac * np.ones(len(similarity.index)), similarity.match_score


def mix_sequences(a: str, b: str, alpha: float) -> str:
    return "".join(
        [
            x if np.random.random() < alpha else y
            for x, y in zip_longest(a, b, fillvalue=" ")
        ]
    ).replace(" ", "")


def describe_plot(
    *, data: pd.DataFrame, x: str, y: str, percentiles: Optional[List[float]] = None,
):
    percentiles = percentiles or [0.25, 0.5, 0.75]
    df = data[[x, y]].groupby(x).describe(percentiles=percentiles)

    # Remove the multi-index from the columns that came from the groupby
    df.columns = df.columns.droplevel(0)

    plt.figure()

    plt.plot(df.index, df["mean"], c="tab:orange", label="Mean")
    plt.fill_between(
        df.index,
        df["mean"] - df["std"],
        df["mean"] + df["std"],
        color="tab:orange",
        alpha=0.25,
        label="Std. Dev.",
    )

    plt.scatter(df.index, df["max"], marker="^", c="tab:gray", label="Max")
    plt.plot(df.index, df["75%"], "-.", c="tab:blue", alpha=0.75, label="75%")
    plt.plot(df.index, df["50%"], c="tab:blue", label="50%")
    plt.plot(df.index, df["25%"], "--", c="tab:blue", alpha=0.75, label="25%")
    plt.scatter(df.index, df["min"], marker="v", c="tab:gray", label="Min")

    plt.xlabel(x)
    plt.ylabel(y)
    plt.legend()


if __name__ == "__main__":
    logging.basicConfig(
        filename=f"../results/probe_global_scores.log",
        filemode="w",
        level=logging.INFO,
    )

    main()
