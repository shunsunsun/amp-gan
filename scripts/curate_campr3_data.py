#!/usr/bin/env python
"""
Cleans the consolidated data obtained from the CAMP_{R3} database.
"""

from pathlib import Path

import pandas as pd


def main():
    df = pd.read_csv("../data/campr3/consolidated.csv", index_col=0)
    print(df.columns)

    # Curate Gram_Nature column
    print(df["Gram_Nature"].value_counts())

    df.loc[df["Gram_Nature"] == "ram+ve", "Gram_Nature"] = "Gram+ve"
    df.loc[df["Gram_Nature"] == "Gram +ve", "Gram_Nature"] = "Gram+ve"

    df.loc[df["Gram_Nature"] == "Gram -ve", "Gram_Nature"] = "Gram-ve"

    df.loc[
        df["Gram_Nature"] == "Gram +ve, Gram -ve", "Gram_Nature"
    ] = "Gram+ve, Gram-ve"
    df.loc[df["Gram_Nature"] == "Gram+,Gram-", "Gram_Nature"] = "Gram+ve, Gram-ve"
    df.loc[df["Gram_Nature"] == "Gram-ve, Gram+ve", "Gram_Nature"] = "Gram+ve, Gram-ve"
    df.loc[df["Gram_Nature"] == "Gram+ve,Gram-ve", "Gram_Nature"] = "Gram+ve, Gram-ve"

    df.loc[df["Gram_Nature"] == " ", "Gram_Nature"] = float("nan")

    print(df["Gram_Nature"].value_counts())

    # Curate Sequence column
    before_sequence_nan = len(df.index)
    df = df[~df["Sequence"].isna()]
    print(f"Rows with NaN Sequence removed: {before_sequence_nan - len(df.index)}")

    # TODO: Handle repeated sequences
    # sequence_counts = df['Sequence'].value_counts()
    # for seq, count in sequence_counts.iteritems():
    #     if count > 2:
    #         print(f'Sequence: {seq}, Count: {count}')
    #         print(df[df['Sequence'] == seq])

    print(
        f'Rows with -NH2 suffix removed: {len(df[df["Sequence"].str.contains("-NH2")])}'
    )
    df = df[~df["Sequence"].str.contains("-NH2")]

    print(
        f'Rows with lower-case sequence characters removed: {df["Sequence"].str.contains("[a-z]", regex=True).sum()}'
    )
    df = df[~df["Sequence"].str.contains("[a-z]", regex=True)]

    print(sorted(set("".join(list(df["Sequence"])))))

    # Curate Source_Organism column
    print(len(df["Source_Organism"].value_counts()))
    df["Source_Organism"] = df["Source_Organism"].str.strip()
    df.loc[
        df["Source_Organism"] == "Odorrana andersonii (golden crossband frog)",
        "Source_Organism",
    ] = "Odorrana andersonii"

    df.loc[
        df["Source_Organism"] == "Anas platyrhynchos (Mallard)", "Source_Organism"
    ] = "Anas platyrhynchos"

    df.loc[
        df["Source_Organism"] == "Androctonus  australis", "Source_Organism"
    ] = "Androctonus australis"

    df.loc[
        df["Source_Organism"] == "Bacillus  subtilis B-TL2", "Source_Organism"
    ] = "Bacillus subtilis B-TL2s"

    df.loc[
        df["Source_Organism"] == "Bos indicus x Bos taurus (hybrid cattle)",
        "Source_Organism",
    ] = "Bos indicus x Bos taurus"

    df.loc[
        df["Source_Organism"] == "Litoria aurea (green and golden bell frog)",
        "Source_Organism",
    ] = "Litoria aurea"

    df.loc[
        df["Source_Organism"] == "Litoria aurea (green and golden bell frog )",
        "Source_Organism",
    ] = "Litoria aurea"

    df.loc[
        df["Source_Organism"] == "Loxodonta africana (African savanna elephant)",
        "Source_Organism",
    ] = "Loxodonta africana"

    df.loc[
        df["Source_Organism"] == "Musca domestica (house fly )", "Source_Organism"
    ] = "Musca domestica"

    df.loc[
        df["Source_Organism"] == "Oryza sativa Japonica Group", "Source_Organism"
    ] = "Oryza sativa subsp. japonica"

    df.loc[
        df["Source_Organism"] == "Petunia hybrida", "Source_Organism"
    ] = "Petunia x hybrida"

    df.loc[
        df["Source_Organism"] == "Rana clamitans (green frog)", "Source_Organism"
    ] = "Rana clamitans"

    df.loc[
        df["Source_Organism"] == "Rana esculenta (edible frog )", "Source_Organism"
    ] = "Rana esculenta"

    df.loc[
        df["Source_Organism"] == "Rana nigromaculata (dark-spotted frog )",
        "Source_Organism",
    ] = "Rana nigromaculata"

    df.loc[
        df["Source_Organism"] == "Streptococcus  pneumoniae", "Source_Organism"
    ] = "Streptococcus  pneumoniae"
    "Streptococcus  pneumoniae"

    print(len(df["Source_Organism"].value_counts()))

    print(df["Activity"].value_counts())
    print(len(df["Activity"].value_counts()))
    df["Activity"] = df["Activity"].fillna("")
    df["Activity"] = df["Activity"].str.lower()
    df["Activity"] = df["Activity"].str.replace(" ", "")
    df["Activity"] = df["Activity"].str.replace(".", ",")
    df["Activity"] = df["Activity"].str.replace("anticancer", "antitumor")
    df["Activity"] = df["Activity"].str.replace("antitumour", "antitumor")

    mapping = {x: ",".join(sorted(x.split(","))) for x in df["Activity"].unique()}
    df["Activity"] = df["Activity"].map(mapping)

    print(df["Activity"].value_counts())
    print(len(df["Activity"].value_counts()))

    temp_file = Path(".temp/source_organism_log.txt")
    temp_file.parent.mkdir(parents=True, exist_ok=True)
    with temp_file.open(mode="w") as f:
        for org in sorted(df["Source_Organism"].dropna().unique()):
            print(org, file=f)


if __name__ == "__main__":
    main()
