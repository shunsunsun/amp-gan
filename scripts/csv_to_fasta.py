import argparse
from pathlib import Path
from typing import Optional

import pandas as pd


def get_parser():
    parser = argparse.ArgumentParser(
        "Converts an CSV file with FASTA sequences into a FASTA file."
    )
    parser.add_argument("-i", "--input_file", type=valid_path)
    parser.add_argument("-o", "--output_file", type=Path, default=None)
    return parser


def valid_path(path: str) -> Path:
    path = Path(path)
    if not path.exists():
        raise FileNotFoundError(str(path))
    return path


def main(input_file: Path, output_file: Optional[Path] = None):
    df = pd.read_csv(input_file)

    output_file = output_file or input_file.with_suffix(".fasta")

    try:
        sequences = df.sequence
    except AttributeError:
        sequences = df.seq

    with output_file.open(mode="w") as f:
        for i, (sequence, target) in enumerate(zip(sequences, df.targets)):
            if target == "[]":
                continue
            f.write(f">> Sequence {i}\n")
            f.write(f"{sequence}\n")


if __name__ == "__main__":
    main(**vars(get_parser().parse_args()))
