#!/usr/bin/env python
"""
Consolidates the data obtained from the CAMP_{R3} database:
    http://www.camp.bicnirrh.res.in
"""


from pathlib import Path

import numpy as np
import pandas as pd


def main():
    dfs = []
    for file in sorted(Path('../data/campr3/raw').glob('*')):
        df = pd.read_csv(file, sep='\t')
        dfs.append(df)
    df = pd.concat(dfs)

    columns = list(df.columns)
    columns[0] = columns[0].strip()
    columns[7] = 'Sequence'
    df.columns = columns

    df.sort_values('Sequence')
    df.set_index = np.arange(len(df.index))

    df.to_csv('../data/campr3/consolidated.csv')


if __name__ == '__main__':
    main()
