#!/bin/bash
# specify a partition
#SBATCH --partition=dggpu
# Request nodes
#SBATCH --nodes=1
# Request some processor cores
#SBATCH --ntasks=4
# Request GPUs
#SBATCH --gres=gpu:1
# Request memory
#SBATCH --mem=32G
# Maximum runtime of 24 hours
#SBATCH --time=24:00:00
# Name of this job
#SBATCH --job-name=amp_gan_train
# Output of this job, stderr and stdout are joined by default
# %x=job-name %j=jobid
#SBATCH --output=../results/%x_%j.out

cd ${SLURM_SUBMIT_DIR}
source ~/.bashrc
conda activate ampgan

/usr/bin/time -v python ../ampgan/train.py -t "${1:-}" -vv
