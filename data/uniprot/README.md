# AMP-GAN Uniprot Data
The data contained in uniprot.tsv was obtained from the following URL:

    https://www.uniprot.org/uniprot/?query=length%3A[1+TO+32]&sort=length

Custom columns were selected using the "columns" button at the top of the data display

 - Entry
 - Entry Name
 - Length
 - Sequence
 - Reviewed/Not Reviewed
 
This selection was then manually downloaded and placed in this directory.
